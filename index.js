let number = Number(prompt("Give me a number: "));
console.log("Your given number is: " + number);

for (let count = number; count >= 0; count--) {

    if (count % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    if (count % 5 == 0) { console.log(count); continue; }
    if (count <= 50) {
        console.log("The number is at 50. Terminating the loop.");
        break;
    }
}



let word = "supercalifragilisticexpialidocious";
let consonant = "";

for (let n = 0; n < word.length; n++) {
    if (
        word[n].toLowerCase() == "a" ||
        word[n].toLowerCase() == "e" ||
        word[n].toLowerCase() == "i" ||
        word[n].toLowerCase() == "o" ||
        word[n].toLowerCase() == "u"
    ) {
        continue;

    } else {
        consonant += word[n];
    };
};

console.log(word);
console.log(consonant);